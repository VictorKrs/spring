package spring.in.action.spintinaction;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest
public class DesignTacoControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void DesignPage() throws Exception {
        mockMvc.perform(get("/design"))
            .andExpect(status().isOk())
            .andExpect(view().name("design"));
    }

    @Test
    public void AddTacoGood() throws Exception {
        mockMvc.perform(post("/design")
            .param("name", "SomeName1")
            .param("ingredients", "FLTO"))
            .andExpect(status().is3xxRedirection())
            .andExpect(model().hasNoErrors())
            .andExpect(redirectedUrl("/orders/current"))
            .andExpect(status().isFound());
    }

    @Test
    public void AddTacoNullNameAndNullIngredients() throws Exception {
        mockMvc.perform(post("/design"))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().attributeHasFieldErrors("taco", "name"))
            .andExpect(model().attributeHasFieldErrors("taco", "ingredients"))
            .andExpect(content().string(containsString("Name must be at least 5 characters long")))
            .andExpect(content().string(containsString("You must choose at least 1 ingredient")));
    }

    @Test
    public void AddTacoEmptyNameAndEmptyIngredients() throws Exception {
        mockMvc.perform(post("/design")
            .param("name", "")
            .param("ingredients", ""))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().attributeErrorCount("taco", 2))
            .andExpect(model().attributeHasFieldErrors("taco", "name"))
            .andExpect(model().attributeHasFieldErrors("taco", "ingredients"))
            .andExpect(content().string(containsString("Name must be at least 5 characters long")))
            .andExpect(content().string(containsString("You must choose at least 1 ingredient")));
    }

    @Test
    public void AddTacoName_4_Length() throws Exception {
        mockMvc.perform(post("/design")
            .param("name", "name")
            .param("ingredients", "SRCR"))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().attributeErrorCount("taco", 1))
            .andExpect(model().attributeHasFieldErrors("taco", "name"))
            .andExpect(content().string(containsString("Name must be at least 5 characters long")));
    }
    
    @Test
    public void AddTacoName_5_Length_IngredientsEmpty() throws Exception {
        mockMvc.perform(post("/design")
            .param("name", "name1")
            .param("ingredients", ""))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().attributeErrorCount("taco", 1))
            .andExpect(model().attributeHasFieldErrors("taco", "ingredients"))
            .andExpect(content().string(containsString("You must choose at least 1 ingredient")));
    }
}
