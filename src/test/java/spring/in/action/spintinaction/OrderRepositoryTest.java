package spring.in.action.spintinaction;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.tomcat.util.buf.StringUtils;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import spring.in.action.spintinaction.interfaces.IngredientRepository;
import spring.in.action.spintinaction.interfaces.OrderRepository;
import spring.in.action.spintinaction.models.Ingredient;
import spring.in.action.spintinaction.models.Taco;
import spring.in.action.spintinaction.models.TacoOrder;
import spring.in.action.spintinaction.models.Ingredient.Type;

// @DataJdbcTest
@SpringBootTest
public class OrderRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private IngredientRepository ingredientRepository;

    // @Test
    public void testAddJDBC(){
        var ingredients = Lists.newArrayList(ingredientRepository.findAll());

        System.out.println("Ingredients count: " + ingredients.size());

        var tacoNew = new Taco();
        tacoNew.setName("testTaco");
        tacoNew.addIngredient(ingredients.get(0));
        
        var tacoOrder = new TacoOrder();
        tacoOrder.addTaco(tacoNew);
        tacoOrder.setDeliveryName("delivName");
        tacoOrder.setDeliveryStreet("delivStreet");
        tacoOrder.setDeliveryCity("delivCity");
        tacoOrder.setDeliveryState("12");
        tacoOrder.setDeliveryZip("1234");
        tacoOrder.setCcNumber("5555555555554444");
        tacoOrder.setCcExpiration("12/27");
        tacoOrder.setCcCvv("123");

        orderRepository.save(tacoOrder);

        var allOrders = orderRepository.findAll();

        for (var order : allOrders){
            System.out.println("Order Id: " + order.getId());
            System.out.println("Tacos: ");
            for (var taco : orderRepository.findAllTacosById(order.getTacoIds())){
                System.out.println("\tName: " + taco.getName());
                System.out.print("\tIngredients: ");
                var ingredientsObjs = ingredientRepository.findAllById(taco.getIngredientsId());
                var ingredientsNames = StreamSupport.stream(ingredientsObjs.spliterator(), false)
                                            .map(Ingredient::getName)
                                            .collect(Collectors.toList());
                System.out.println(StringUtils.join(ingredientsNames, ','));
            }
        }
    }

    @Test
    public void addIngredient(){
        var ingredient = new Ingredient("Test", "Test", Type.CHEESE);

        ingredientRepository.save(ingredient);

        var result = ingredientRepository.findById(ingredient.getId());

        System.out.println("Result: " + result);

        assertNotNull(result);
    }

    @Test
    public void testFindIngredients(){
        var ingredients = Lists.newArrayList(ingredientRepository.findAll());

        System.out.println("Ingredients count: " + ingredients.size());

        assertTrue(ingredients.size() > 0);
    }

    @Test
    public void testAddJPA(){
        var ingredients = Lists.newArrayList(ingredientRepository.findAll());

        System.out.println("Ingredients count: " + ingredients.size());
        
        var orderCount = orderRepository.count();
        
        var tacoNew = new Taco();
        tacoNew.setName("testTaco" + orderCount);
        tacoNew.addIngredient(ingredients.get(0));
        
        var tacoOrder = new TacoOrder();
        tacoOrder.addTaco(tacoNew);
        tacoOrder.setDeliveryName("delivName"  + orderCount);
        tacoOrder.setDeliveryStreet("delivStreet" + orderCount);
        tacoOrder.setDeliveryCity("delivCity" + orderCount);
        tacoOrder.setDeliveryState("12");
        tacoOrder.setDeliveryZip("1234");
        tacoOrder.setCcNumber("5555555555554444");
        tacoOrder.setCcExpiration("12/27");
        tacoOrder.setCcCvv("123");

        orderRepository.save(tacoOrder);

        checkTacoOrder(tacoOrder);
    }

    public <T> boolean checkValues(T val, T dbVal, String name){
        if (val.equals(dbVal)) return true;
        
        System.err.println(name + " not equals; Val: " + val + "; DbVal: " + dbVal);

        return false;
    }

    public void checkTacoOrder(TacoOrder order){
        var orderDB = orderRepository.findByDeliveryName(order.getDeliveryName()).get(0);
        
        var isOk = true;

        isOk = checkValues(order.getDeliveryName(), orderDB.getDeliveryName(), "DeliveryName") && isOk;
        isOk = checkValues(order.getDeliveryCity(), orderDB.getDeliveryCity(), "DeliveryCity") && isOk;
        isOk = checkValues(order.getDeliveryStreet(), orderDB.getDeliveryStreet(), "DeliveryStreet") && isOk;
        isOk = checkValues(order.getDeliveryState(), orderDB.getDeliveryState(), "DeliveryState") && isOk;
        isOk = checkValues(order.getDeliveryZip(), orderDB.getDeliveryZip(), "DeliveryZip") && isOk;
        isOk = checkValues(order.getCcCvv(), orderDB.getCcCvv(), "CcCvv") && isOk;
        isOk = checkValues(order.getCcExpiration(), orderDB.getCcExpiration(), "CcExpiration") && isOk;
        isOk = checkValues(order.getCcNumber(), orderDB.getCcNumber(), "CcNumber") && isOk;
        var tacosSizeIsOk = checkValues(order.getTacos().size(), orderDB.getTacos().size(), "TacosCount") && orderDB.getTacos().size() > 0;
        isOk = tacosSizeIsOk && isOk;

        if (tacosSizeIsOk){
            var tacosDBIt = orderDB.getTacos().iterator();
            var tacosIt = order.getTacos().iterator();

            while (tacosDBIt.hasNext()) {
                var tacoDb = tacosDBIt.next();
                var taco = tacosIt.next();

                isOk = checkValues(taco.getName(), tacoDb.getName(), "TacoName") && isOk;
                var ingCountIsOk = checkValues(taco.getIngredients().size(), tacoDb.getIngredients().size(), "TacoIngredientsCount") && tacoDb.getIngredients().size() > 0;
                isOk = ingCountIsOk && isOk;

                if (ingCountIsOk){
                    var ingDbIt = tacoDb.getIngredients().iterator();
                    var ingIt = taco.getIngredients().iterator();

                    while(ingDbIt.hasNext()){
                        var ingDb = ingDbIt.next();
                        var ing = ingIt.next();

                        isOk = checkValues(ing.getName(), ingDb.getName(), "IngName") && isOk;
                        isOk = checkValues(ing.getType(), ingDb.getType(), "IngType") && isOk;
                    }
                }
                else {
                    System.err.println("ingredientCount is 0");
                }
            }
        }
        else {
            System.err.println("tacoCount is 0");
        }

        if (isOk){
            System.out.println("Is Equals!");
        } else {
            System.err.println("Not Equals");
        }
    }

    public void findAllTacos(){
        var allOrders = orderRepository.findAll();

        for (var order : allOrders){
            System.out.println("Order Id: " + order.getId() + 
                                "\n\t" + "DeliveryName: " + order.getDeliveryName() +
                                "\n\t" + "DeliveryStreet: " + order.getDeliveryStreet() +
                                "\n\t" + "DeliveryCity: " + order.getDeliveryCity() +
                                "\n\t" + "DeliveryState: " + order.getDeliveryState() +
                                "\n\t" + "DeliveryZip: " + order.getDeliveryZip() +
                                "\n\t" + "CC_Number: " + order.getCcNumber() +
                                "\n\t" + "CC_Expiration: " + order.getCcExpiration() +
                                "\n\t" + "CC_CVV: " + order.getCcCvv() +
                                "\n\t" + "PlacedAt: " + order.getPlacedAt() +
                                "\n\t" + "UserId: " + order.getUser()
                            );

            var tacos = order.getTacos();
            System.out.println("\t" + "Tacos count: " + tacos.size());
            for (var taco : tacos){
                System.out.println("\t\t" + "Taco Name: " + taco.getName());
                var ingredientsNames = taco.getIngredients().stream()
                                            .map(Ingredient::getName)
                                            .collect(Collectors.toList());  
                
                System.out.println("\t\t\t" + "Ingredients: " + StringUtils.join(ingredientsNames, ','));
            }
        }
    }

}
