package spring.in.action.spintinaction;

import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import spring.in.action.spintinaction.models.Ingredient;

@SpringBootTest
public class RestTest {

    String baseUrl = "http://localhost:8080/api";
    RestTemplate rest = new RestTemplate();

    @Test
    public void GetIngredient(){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var httpEntity = new HttpEntity<String>(headers);
        
        var url = baseUrl + "/ingredients/{id}";

        // var ing = rest.getForObject(url, Ingredient.class, "COTO");
        // System.out.println("GetForObject: " + ing.toString());
        
        var urlVars = new HashMap<String, String>();
        urlVars.put("id", "COTO");

        // var respEntity = rest.getForEntity(url, Ingredient.class, urlVars);
        // System.out.println("getForEntity: " + respEntity.toString());

        var ing = rest.exchange(url, HttpMethod.GET, httpEntity, Ingredient.class, "COTO");
        System.out.println("Exchange: " + ing.toString());
    }
}
