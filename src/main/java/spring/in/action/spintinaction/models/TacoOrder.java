package spring.in.action.spintinaction.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import io.micrometer.common.util.StringUtils;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

// import org.hibernate.validator.constraints.CreditCardNumber;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
public class TacoOrder {

    @SuppressWarnings("unused")
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private UserT user;
    
    private Date placedAt = new Date();
    
    @NotBlank(message = "Delivery name is required")
    private String deliveryName;

    @NotBlank(message = "Delivery street is required")
    private String deliveryStreet;

    @NotBlank(message = "Delivery city is required")
    private String deliveryCity;

    @Pattern(regexp = "^\\d{1,2}$", message="Delivery state must be from 1 to 2 digits")
    private String deliveryState;

    @Pattern(regexp = "^\\d{1,10}$", message="Delivery Zip must be from 1 to 10 digits")
    private String deliveryZip;

    //@CreditCardNumber(message = "Not a valid credit card number")
    @Pattern(regexp = "^\\d{16}$", message="Not a valid credit card number")
    private String ccNumber;

    @Pattern(regexp = "^(0[1-9]|1[0-2])([\\/])([2-9][0-9])$", message = "Must be formatted MM/YY")
    private String ccExpiration;

    @Pattern(regexp = "\\d{3}", message = "Invalid CVV")
    private String ccCvv;

    @NotNull
    @Size(min=1, message = "You must add at least 1 taco")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Taco> tacos = new ArrayList<>();

    public void addTaco(Taco taco){
        tacos.add(taco);
    }

    public Set<Long> getTacoIds(){
        return this.tacos.stream()
            .map(Taco::getId)
            .collect(Collectors.toSet());
    }

    public void setUser(UserT user){
        this.user = user;

        updateValue(deliveryName, user.getUsername(), this::setDeliveryName);
        updateValue(deliveryStreet, user.getStreet(), this::setDeliveryStreet);
        updateValue(deliveryCity, user.getCity(), this::setDeliveryCity);
        updateValue(deliveryState, user.getState(), this::setDeliveryState);
        updateValue(deliveryZip, user.getZip(), this::setDeliveryZip);

        System.err.println("user: " + user);
    }

    private void updateValue(String src, String dest, Consumer<String> setter){
        if (src == null || StringUtils.isBlank(src)){
            setter.accept(dest);
        }
    }
}
