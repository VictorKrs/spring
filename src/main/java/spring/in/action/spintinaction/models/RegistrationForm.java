package spring.in.action.spintinaction.models;

import org.springframework.security.crypto.password.PasswordEncoder;

import jakarta.validation.constraints.Pattern;
import lombok.Data;
import spring.in.action.spintinaction.annotations.UniqueUsernameConstraint;

@Data
public class RegistrationForm {
    
    @Pattern(regexp = "^[\\w]{5,15}$", message="Username must be from 5 to 15 latin characters or digits")
    @UniqueUsernameConstraint(message="User with tha same username already exists")
    private String username;
    @Pattern(regexp = "^[\\w]{5,15}$", message="Password must be from 5 to 15 latin characters or digits")
    private String password;
    @Pattern(regexp = "^[a-zA-Z ]{5,50}$", message="Fullname must be from 5 to 50 characters")
    private String fullname;
    @Pattern(regexp = "^[\\w]{5,50}$", message="Street must be from 5 to 50 characters")
    private String street;
    @Pattern(regexp = "^[\\w]{5,50}$", message="City must be from 5 to 50 characters")
    private String city;
    @Pattern(regexp = "^\\d{1,2}$", message="State must be from 1 to 2 digits")
    private String state;
    @Pattern(regexp = "^\\d{1,10}$", message="Zip must be from 1 to 10 digits")
    private String zip;
    @Pattern(regexp = "^\\d{11,25}$", message="Phone number must be from 11 to 25 digits")
    private String phone;

    public UserT toUser(PasswordEncoder passwordEncoder) {
        return new UserT(username, passwordEncoder.encode(password), fullname, street, city, state, zip, phone);
    }
}
