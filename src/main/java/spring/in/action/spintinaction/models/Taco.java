package spring.in.action.spintinaction.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
public class Taco {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date createdAt = new Date();
    
    @NotNull(message = "Name must be at least 5 characters long")
    @Size(min=5, message = "Name must be at least 5 characters long")
    private String name;

    @NotNull(message = "You must choose at least 1 ingredient")
    @Size(min=1, message = "You must choose at least 1 ingredient")
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Ingredient> ingredients = new ArrayList<>();

    public void addIngredient(Ingredient ingredient){
        // ingredients.add(new IngredientRef(ingredient.getId(), this.ingredients.size()));
        ingredients.add(ingredient);
    }

    public Set<String> getIngredientsId(){
        return this.ingredients.stream()
            .map(Ingredient::getId)
            // .map(IngredientRef::getIngredient)
            .collect(Collectors.toSet());
    }
}
