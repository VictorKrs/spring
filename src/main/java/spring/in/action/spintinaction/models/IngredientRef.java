package spring.in.action.spintinaction.models;

import lombok.Data;

@Data
public class IngredientRef {

    //AggregateReference<Taco, Long> taco;

    private String ingredient;

    public IngredientRef(String ingredient) {
        this.ingredient = ingredient;     
    }
    // private final Long taco;
    // private final String ingredient;

}
