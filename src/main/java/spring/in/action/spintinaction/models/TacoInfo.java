package spring.in.action.spintinaction.models;

import java.util.List;

import lombok.Data;

@Data
public class TacoInfo {
    
    private Long id;
    private String name;
    private List<Ingredient> ingredients; 

    public TacoInfo(Taco taco, List<Ingredient> ingredients) {
        this.name = taco.getName();
        this.ingredients = ingredients;
    }
}
