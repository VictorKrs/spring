package spring.in.action.spintinaction.interfaces;

import java.util.Collection;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import spring.in.action.spintinaction.models.Taco;
import spring.in.action.spintinaction.models.TacoOrder;
import java.util.List;


public interface OrderRepository extends CrudRepository<TacoOrder, Long> {

    @Query(value = "select t from Taco t where t.id in :ids")
    Set<Taco> findAllTacosById(@Param("ids") Collection<Long> ids);

    List<TacoOrder> findByDeliveryZip(String deliveryZip);

    List<TacoOrder> findByDeliveryName(String deliveryName);
}
