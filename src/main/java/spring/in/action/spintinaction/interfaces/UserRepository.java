package spring.in.action.spintinaction.interfaces;

import org.springframework.data.repository.CrudRepository;

import spring.in.action.spintinaction.models.UserT;

public interface UserRepository extends CrudRepository<UserT, Long> {

    UserT findByUsername(String username);
}
