package spring.in.action.spintinaction.interfaces;

import org.springframework.data.repository.CrudRepository;

import spring.in.action.spintinaction.models.Ingredient;

public interface IngredientRepository extends CrudRepository<Ingredient, String> {

    // Iterable<Ingredient> findAll();

    // Optional<Ingredient> findById(String id);

    // Ingredient save(Ingredient ingredient);
}
