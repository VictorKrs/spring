package spring.in.action.spintinaction.annotations;

import org.springframework.beans.factory.annotation.Autowired;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import spring.in.action.spintinaction.interfaces.UserRepository;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsernameConstraint, String>{

    @Autowired
    UserRepository userRepository;

    @Override
    public void initialize(UniqueUsernameConstraint username){
    }

    @Override
    public boolean isValid(String username, ConstraintValidatorContext cxt) {
        return (userRepository.findByUsername(username) == null);
    }

}
