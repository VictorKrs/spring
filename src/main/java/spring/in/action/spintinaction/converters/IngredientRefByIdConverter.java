package spring.in.action.spintinaction.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import spring.in.action.spintinaction.models.IngredientRef;

@Component
public class IngredientRefByIdConverter implements Converter<String, IngredientRef> {

    @Override
    public IngredientRef convert(String ingredientId) {
        // return new IngredientRef(ingredientId, 0);
        return new IngredientRef(ingredientId);
    }
}
