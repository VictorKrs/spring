package spring.in.action.spintinaction.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import spring.in.action.spintinaction.interfaces.IngredientRepository;
import spring.in.action.spintinaction.models.Ingredient;

@Component
public class IngredientByIdConverter implements Converter<String, Ingredient> {

    @Autowired
    private  IngredientRepository ingredientRepo;

    @Override
    public Ingredient convert(String id) {
        return ingredientRepo.findById(id).orElse(null);     
    }
}