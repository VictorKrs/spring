package spring.in.action.spintinaction.configurations;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import lombok.extern.slf4j.Slf4j;
import spring.in.action.spintinaction.interfaces.UserRepository;
import spring.in.action.spintinaction.models.UserT;

@Slf4j
@Configuration
public class SecurityConfig {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    // Users IN MEMORY
    // @Bean
    // public UserDetailsService userDetailsService(PasswordEncoder encoder){
    //     List<UserDetails> usersList = new ArrayList<>();

    //     usersList.add(new User("buss", encoder.encode("password"), Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"))));
    //     usersList.add(new User("woody", encoder.encode("password"), Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"))));

    //     return new InMemoryUserDetailsManager(usersList);
    // }

    @Bean
    public UserDetailsService userDetailsService(UserRepository userRepo){
        return username -> {
            log.info("UserDetailService; Username: " + username);
            UserT user = userRepo.findByUsername(username);

            if (user != null) return user;
            log.info("User not found; Username: " + username);

            throw new UsernameNotFoundException("User '" + username + "' not found");
        };
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        http.authorizeHttpRequests((authz) -> authz
            .requestMatchers("/design", "/orders", "/orders/*").hasRole("USER")
            .requestMatchers("/", "/**").permitAll());

        http.csrf(AbstractHttpConfigurer::disable);

        http.formLogin(formLogin ->
                formLogin.loginPage("/login")
                    .permitAll());

        return http.build();
    }
}
