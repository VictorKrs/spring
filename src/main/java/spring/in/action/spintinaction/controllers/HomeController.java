package spring.in.action.spintinaction.controllers;

import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;
import spring.in.action.spintinaction.configProps.TestProps;

@Slf4j
@Controller
@RequestMapping("/")
public class HomeController {
    
    TestProps testProp;

    public HomeController(TestProps prop) {
        testProp = prop;
    }

    @GetMapping
    public String home() {
        log.info("TestProp: {}", testProp);

        return "home";
    }
}
