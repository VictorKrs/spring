package spring.in.action.spintinaction.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import spring.in.action.spintinaction.interfaces.IngredientRepository;

@RestController
@RequestMapping(path="/api/ingredients", produces="application/json")
@CrossOrigin(origins="http://tacocloud:8080")
public class IngredientController {

    IngredientRepository ingRepo;
    
    public IngredientController(IngredientRepository ingRepo) {
        this.ingRepo = ingRepo;
    }

    @GetMapping(path="/{id}", consumes="application/json")
    public ResponseEntity<Object> GetIngredientById(@PathVariable("id") String id){
        var result = ingRepo.findById(id);

        if (result.isPresent()){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>("Ingredient with id=" + id + " not found", HttpStatus.NOT_FOUND);
    }
}
