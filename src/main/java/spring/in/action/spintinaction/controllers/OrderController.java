package spring.in.action.spintinaction.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import spring.in.action.spintinaction.interfaces.IngredientRepository;
import spring.in.action.spintinaction.interfaces.OrderRepository;
import spring.in.action.spintinaction.models.Ingredient;
// import spring.in.action.spintinaction.models.IngredientRef;
import spring.in.action.spintinaction.models.Taco;
import spring.in.action.spintinaction.models.TacoInfo;
import spring.in.action.spintinaction.models.TacoOrder;

import org.springframework.web.bind.annotation.RequestMapping;


@Slf4j
@Controller
@RequestMapping("/orders")
@SessionAttributes("tacoOrder")
public class OrderController {
    
    @Autowired
    public OrderRepository orderRepo;

    @Autowired
    private IngredientRepository ingredientRepo;

    @ModelAttribute
    public void AddTacosInfo(Model model){
        var tacoOrder = (TacoOrder)model.getAttribute("tacoOrder");

        if (tacoOrder == null){
            log.info("tacoOrder attribute is null");
            return;
        }

        var tacosInfo = new ArrayList<TacoInfo>();
        for (var taco : tacoOrder.getTacos()) {
            var ingredients = getTacoIngredients(taco);
            
            tacosInfo.add(new TacoInfo(taco, ingredients));
        }

        model.addAttribute("tacosInfo", tacosInfo);
    }

    private List<Ingredient> getTacoIngredients(Taco taco){
        var ingredientsIds = taco.getIngredients().stream()
            .map(Ingredient::getId)
            // .map(IngredientRef::getIngredient)
            .collect(Collectors.toList());

        var ingredients = new ArrayList<Ingredient>();
        ingredientRepo.findAllById(ingredientsIds).forEach(ingredients::add);
    
        return ingredients;
    }

    @ModelAttribute(name="tacosInfo")
    public List<TacoInfo> tacosInfo(){
        return new ArrayList<TacoInfo>();
    }

    @GetMapping("/current")
    public String orderForm() {
        return "orderForm"; 
    }

    @GetMapping
    public String rediretcToOrderForm(){
        return "redirect:/orders/current";
    }
    
    @PostMapping
    public String processOrder(
        @Valid TacoOrder tacoOrder,
        Errors errors,
        SessionStatus sessionStatus){
        
        if(errors.hasErrors()){
            log.info("hasErrors: {}", errors);
            return "orderForm";
        }

        log.info("Order submitted: {}", tacoOrder);

        // SetTacosKey
        // for (Taco taco : tacoOrder.getTacos()) {
        //     long i = 0;
        //     for (IngredientRef ingredients : taco.getIngredients()) {
        //         ingredients.setTacoKey(i++);
        //     }
        // }

        try {
            orderRepo.save(tacoOrder);
            sessionStatus.setComplete();
            
            return "redirect:/";
        } catch (Exception e){
            log.error("Exception insert tacoOrder", e);
            return "orderForm";
        }

    }
}
