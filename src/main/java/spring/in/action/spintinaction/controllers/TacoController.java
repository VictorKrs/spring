package spring.in.action.spintinaction.controllers;

import org.springframework.web.bind.annotation.RestController;

import spring.in.action.spintinaction.interfaces.TacoRepository;
import spring.in.action.spintinaction.models.Taco;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping(path="/api/tacos", produces="application/json")
@CrossOrigin(origins="http://tacocloud:8080")
public class TacoController {

    private TacoRepository tacoRepo;

    public TacoController(TacoRepository tacoRepo){
        this.tacoRepo = tacoRepo;
    }

    @GetMapping(params="recent")
    public Iterable<Taco> recentTacos() {
        PageRequest page = PageRequest.of(0, 12, Sort.by("createdAt").descending());

        return tacoRepo.findAll(page).getContent();
    }   

    @GetMapping("/{id}")
    public ResponseEntity<Object> tacoById(@PathVariable("id") Long id) {
        var taco = tacoRepo.findById(id);

        if (taco.isPresent()){
            return new ResponseEntity<>(taco.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>("Taco with ID=" + id + " not found;", HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(consumes="application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Taco postMethodName(@RequestBody Taco taco) {
        
        return tacoRepo.save(taco);
    }
    
    
}
