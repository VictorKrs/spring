package spring.in.action.spintinaction.controllers;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import lombok.extern.slf4j.Slf4j;
import spring.in.action.spintinaction.interfaces.IngredientRepository;
import spring.in.action.spintinaction.models.Ingredient;
import spring.in.action.spintinaction.models.Taco;
import spring.in.action.spintinaction.models.TacoOrder;
import spring.in.action.spintinaction.models.UserT;
import spring.in.action.spintinaction.models.Ingredient.Type;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;



@Slf4j
@Controller
@RequestMapping("/design")
@SessionAttributes("tacoOrder")
public class DesignTacoController {

    @Autowired
    private IngredientRepository ingredientRepo;

    @ModelAttribute
    public void addIngredientsToModel(Model model){
        Iterable<Ingredient> ingredients = ingredientRepo.findAll();

        var types = Ingredient.Type.values();

        for (var type: types){
            model.addAttribute(type.toString().toLowerCase(),
                filterByType(ingredients, type));
        }
    }

    @ModelAttribute(name = "tacoOrder")
    public TacoOrder order(@AuthenticationPrincipal UserT user) {
        var result = new TacoOrder();
        result.setUser(user);
        return result;
    }

    @ModelAttribute(name = "taco")
    public Taco taco (){
        return new Taco();
    }

    @GetMapping
    public String showDesignForm() {
        return "design";
    }

    private Iterable<Ingredient> filterByType(Iterable<Ingredient> ingredients, Type type) {
        return StreamSupport.stream(ingredients.spliterator(), false)
                .filter(x -> x.getType().equals(type))
                .collect(Collectors.toList());
    }

    @PostMapping
    public String processTaco(
                    @Valid Taco taco,
                    Errors errors,
                    @ModelAttribute TacoOrder tacoOrder) {
        
        if (errors.hasErrors()){
            log.info("Design has errors");
            return "design";
        }

        log.info("Processing taco: {}", taco);
        tacoOrder.addTaco(taco);

        return "redirect:/orders/current";
    }
}
