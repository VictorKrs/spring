package spring.in.action.spintinaction.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import spring.in.action.spintinaction.interfaces.UserRepository;
import spring.in.action.spintinaction.models.RegistrationForm;

@Slf4j
@Controller
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @ModelAttribute(name = "userInfo")
    public RegistrationForm userInfo(){
        log.info("Create model attribute");
        return new RegistrationForm();
    }

    @GetMapping
    public String registerForm(){
        return "registration";
    }

    @PostMapping
    public String processRegistration(
                    @Valid @ModelAttribute("userInfo") RegistrationForm userInfo, 
                    Errors errors){
        if (errors.hasErrors()){
            log.info("hasErrors: {}", errors);
            return "registration";
        }
        
        try{
            userRepo.save(userInfo.toUser(passwordEncoder));
        }
        catch (Exception e){
            log.error("Error register user", e);
        }

        return "redirect:/login";
    }
}
