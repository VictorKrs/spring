create table if not exists usert(
    id bigserial,
    city varchar(50) not null,
    fullname varchar(50) not null,
    password varchar(255) not null,
    phone_number varchar(20) not null,
    state varchar(2) not null,
    street varchar(50) not null,
    username varchar(25) not null,
    zip varchar(10) not null,
    primary key (id)
);

create table if not exists taco_order(
    id bigserial,
    placed_at timestamp not null,
    user_id bigint not null,
    cc_cvv varchar(3) not null,
    cc_expiration timestamp not null,
    cc_number varchar(20) not null,
    delivery_city varchar(50) not null,
    delivery_state varchar(2) not null,
    delivery_street varchar(50) not null,
    delivery_zip varchar(10) not null,
    primary key(id),
    constraint fk_user foreign key(user_id) references usert(id)
);

create table if not exists ingredient(
    id varchar(20) not null,
    name varchar(50) not null,
    type varchar(50) not null,
    primary key(id)
);

create table if not exists taco(
    id bigserial,
    name varchar(50) not null,
    created_at timestamp not null,
    primary key(id)
);

create table if not exists taco_ingredients(
    taco_id bigint not null,
    ingredients_id varchar(20) not null,
    constraint fk_taco foreign key(taco_id) references taco(id),
    constraint fk_ingredient foreign key(ingredients_id) references ingredient(id)
);

create table if not exists taco_order_tacos(
    taco_order_id bigint not null,
    tacos_id bigint not null,
    constraint fk_taco_order foreign key(taco_order_id) references taco_order(id),
    constraint fk_taco foreign key(tacos_id) references taco(id)
);